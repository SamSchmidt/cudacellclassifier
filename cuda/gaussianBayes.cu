/* CUDA CellClassifier - Classify cells in parallel using CUDA
Copyright(C) <2015>  <Samuel Schmidt>

This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>
*/

//gaussianBayes.cu
//
//This file contains all the CUDA functions and the host code that calls them.
#include "../cudaHeaders.h"

//In general, array lengths are denoted by:
//NUM_SAMPLES	  s
//NUM_FEATURES	  f
//NUM_CLASSES	  c


/////////////
///GLOBALS///
/////////////
double* d_data;			//the data array - s x f
int* d_classes;			//classes of each sample in order - c

double* d_means;		//means for class-feature pairs - c x f
double* d_variances;	//var. for class-feature pairs - c x f
double* d_pValues;		//probability of each class - c
extern double* d_testData;		//vector to classify - f

double2* d_gaussianProbs;
double2* d_finalProbs;

double2* d_reduce_intermediate;


///////////////////////
///UTILITY FUNCTIONS///
///////////////////////

//Utility testing function to compare two arrays.
//Returns nothing, prints a true/false value.
void compareArrays(double** a, double* b, int size1, int size2)
{
	bool equal = true;

	//printf("compare\n");
	for (int i = 0; i < size2; ++i)
	{
		for (int j = 0; j < size1;++j)
		{
			int idx = i*size1 + j;
			
			if (a[j][i] != b[idx])
			{
				//printf("Arrays not equal at %d:\t%f\t%f\n", idx, a[j][i], b[idx]);
				equal = false;
			}
		}
	}

	printf("compare equal?: %d", equal);
}

/*
 * CUDAmalloc and memcpy the vars we need for CUDA classification
 */
void initializeCudaVars(NaiveBayes nb){

	//Malloc device vars
	cudaErrorCheck(cudaMalloc(&d_data, NUM_FEATURES * NUM_SAMPLES * sizeof(double)));
	cudaErrorCheck(cudaMalloc(&d_classes, NUM_SAMPLES * sizeof(int)));
	cudaErrorCheck(cudaMalloc(&d_means, NUM_CLASSES * NUM_FEATURES * sizeof(double)));
	cudaErrorCheck(cudaMalloc(&d_variances, NUM_CLASSES * NUM_FEATURES * sizeof(double)));
	cudaErrorCheck(cudaMalloc(&d_pValues, NUM_CLASSES * NUM_FEATURES * sizeof(double)));
	
	cudaErrorCheck(cudaMalloc(&d_gaussianProbs, NUM_CLASSES * NUM_FEATURES * sizeof(double2)));
	cudaErrorCheck(cudaMalloc(&d_finalProbs, NUM_CLASSES * sizeof(double2)));
	
	cudaErrorCheck(cudaMalloc(&d_testData, NUM_FEATURES * sizeof(double)));
	
	//Copy vars from serial work that we need
	cudaErrorCheck(cudaMemcpy(d_data, nb.get1Ddata(), NUM_FEATURES * NUM_SAMPLES * sizeof(double), cudaMemcpyHostToDevice));
	cudaErrorCheck(cudaMemcpy(d_classes, nb.getClasses(), NUM_SAMPLES * sizeof(int), cudaMemcpyHostToDevice));
}


///////////////
///HOST CODE///
///////////////
/*
 * Calculate the means and variances of each feature-class pair using CUDA
 */
void classifierStats()
{
	//Set block and grid size:
	dim3 blockSize(BLOCK_SIZE, 1, 1);
	int gridX = NUM_FEATURES / blockSize.x + 1;	//+1 assures we round up (ceil not working)
	dim3 gridSize(gridX, 1, 1);
	
	//Calculate means	
	meanByFeature<<<gridSize, blockSize, NUM_SAMPLES * sizeof(int) >> >(d_data, d_means, d_classes/*, NUM_CLASSES, NUM_SAMPLES, NUM_FEATURES*/);
	cudaErrorCheck(cudaGetLastError());

	//Calculate variances
	varianceByFeature<<<gridSize, blockSize, NUM_SAMPLES * sizeof(int)>>>( d_data, d_variances, d_means, d_classes/*, NUM_CLASSES, NUM_SAMPLES, NUM_FEATURES*/);
	cudaErrorCheck(cudaDeviceSynchronize());
	cudaErrorCheck(cudaGetLastError());
}

/*
 * Classify the input vector (currently found in d_testData)
 * classProbs is the prior probability of each class (# occurrences/#occurrences of all classes)
 * return the fuzzy set of probabilities in h_fuzzy

 * RETURNS the 0-indexed class with the greatest probability
 */
//TODO eventually this should be flexible and allow an input or perhaps a number of inputs
int gaussianClassify(double2 * h_fuzzy, double* classProbs, NaiveBayes nb, double* h_testData)
{
	double* h_variances = new double[NUM_CLASSES*NUM_FEATURES];
	cudaErrorCheck(cudaMemcpy(h_variances, d_variances, NUM_FEATURES*NUM_CLASSES*sizeof(double), cudaMemcpyDeviceToHost));
	cudaErrorCheck(cudaMemcpy(d_testData, h_testData, NUM_FEATURES * sizeof(double), cudaMemcpyHostToDevice));
	
	//compareArrays(nb.variances, h_variances, NUM_CLASSES, NUM_FEATURES);

	//Set block and grid size:
	dim3 blockSize(BLOCK_SIZE, 1, 1);
	int gridX = NUM_FEATURES / blockSize.x + 1;	//+1 assures we round up (ceil not working)
	dim3 gridSize(gridX, 1, 1);


	//Get the probabilities of each class (without the priors)
	gaussianClassifierByFeature <<<gridSize, blockSize>>>(d_testData, d_gaussianProbs, d_means, d_variances);
	cudaDeviceSynchronize();

	//double2* h_gaussianProbs = new double2[100];
	//cudaErrorCheck(cudaMemcpy(h_gaussianProbs, d_gaussianProbs, 100*sizeof(double2), cudaMemcpyDeviceToHost));

	//New blocksize and gridsize
	blockSize.x = 1024;
	gridSize.x = (NUM_FEATURES / blockSize.x) + 1; //+1 assures we round up (ceil not working)

	//Malloc an temporary array to hold intermediate reduce results
	cudaErrorCheck(cudaMalloc(&d_reduce_intermediate, gridSize.x * sizeof(double2)));

	//For each class, perform reduces that distill our large arrays of probabilities down to single values
	for (int i = 0; i < NUM_CLASSES; ++i)
	{
		//New blocksize and gridsize
		blockSize.x = 1024;
		gridSize.x = (NUM_FEATURES / blockSize.x)/2 + 1; //+1 assures we round up (ceil not working)
		//NEW halve the number of blocks


		//1. Reduce from NUM_FEAUTURES to 1024
		reduceToClassProbability <<<gridSize, blockSize, blockSize.x * sizeof(double2) >>>(&d_gaussianProbs[NUM_FEATURES*i], d_reduce_intermediate, NUM_FEATURES);
		cudaDeviceSynchronize();

		double2* h_intermediate = new double2[gridSize.x];
		//debug check intermediate array
		cudaErrorCheck(cudaMemcpy(h_intermediate, d_reduce_intermediate, gridSize.x*sizeof(double2), cudaMemcpyDeviceToHost));

		//2. Reduce from 1024 to 1
		unsigned int newBlockSize = 2;
		while (newBlockSize < gridSize.x)
			newBlockSize *= 2;
		blockSize.x = newBlockSize/2;  //new for the speed thing

		reduceToClassProbability <<<1, blockSize, blockSize.x * sizeof(double2) >>>(d_reduce_intermediate, &d_finalProbs[i], gridSize.x);
		cudaErrorCheck(cudaDeviceSynchronize());
	}

	//Copy device results to host (for test)
	cudaErrorCheck(cudaMemcpy(h_fuzzy, d_finalProbs, NUM_CLASSES*sizeof(double2), cudaMemcpyDeviceToHost));
	cudaErrorCheck(cudaDeviceSynchronize());

	//Find the max of the NUM_CLASSES probabilities
	//only 114 classes for now so we can do it all in one block
	double2* max;
	int* d_index;
	cudaErrorCheck(cudaMalloc(&max,sizeof(double2)));
	cudaErrorCheck(cudaMalloc(&d_index, sizeof(int)));

	//Make blockSize the smallest power of 2 that is >NUM_CLASSES
	for (blockSize.x = 1; blockSize.x < NUM_CLASSES; blockSize.x *= 2){}
	blockSize.x = 128;
	findMaxProbability<<<1, blockSize, NUM_CLASSES*sizeof(double2)>>>(d_finalProbs, max, d_index);

	//copy index of class w/ highest prob over to host
	int* h_index = new int;
	cudaErrorCheck(cudaMemcpy(h_index, d_index, sizeof(int), cudaMemcpyDeviceToHost));

	//printf("final class:   %d", *h_index);

	delete[] h_variances;

	return *h_index+1;
}


//////////////////
///CUDA KERNELS///
//////////////////

/*
 * Calculates the mean of each feature-class pair.
 * Puts the results in d_output.
 * d_output is arranged so the f-c mean is at the index f*NUM_CLASSES + c (c x f)

 * inputs:
 *  d_input is the data: s x f
 *  classes: c
 */
__global__
void meanByFeature(double * d_input, double * d_output, int * classes/*, int numClasses, int numSamples, int numFeatures*/)
{ 
	//classes input are 1-indexed, convert to 0-index
	
	//Class # of each sample for fast access
	__shared__ int sharedClasses[NUM_SAMPLES];
		
	//Copy classes to shared memory, convert from 1 to 0 indexing
	for (int i = threadIdx.x; i < NUM_SAMPLES; i += blockDim.x)
	{
		sharedClasses[i] = classes[i] - 1;
	}

	__syncthreads();

	//Index from 1 to NUM_FEATURES ie the feature this thread is concerned with
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	//if outside the bounds of the input array (extra threads are allocated to make sure we have enough)
	if (idx >= NUM_FEATURES)
		return;
	
	//Don't use 'new' here, unnecessary
	double sums[NUM_CLASSES];			//Array that holds the sum for each class. 
	double sumCounts[NUM_CLASSES];		//Array that holds the count for each class.

	//INITIALIZE CLASS SUMS
	for( int i = 0; i < NUM_CLASSES; ++i)
	{
		sums[i] = 0.0;
		sumCounts[i] = 0.0;
	}

	//Add up the sums of each feature-class pair
	//Also count the occurences of each feature-class pair
	for( int i = 0; i < NUM_SAMPLES; ++i)
	{
		sums[sharedClasses[i]] += d_input[idx*NUM_SAMPLES + i]; //Add the current sample to the sum for its class.
		sumCounts[sharedClasses[i]]++;		//Increment the number of samples in the current sample's class.
	}

	//Insert the means into the output array
	for (int i = 0; i < NUM_CLASSES; ++i)
	{
		double val = 0;
		if (sumCounts[i] > 0)
		{
			val = sums[i] / sumCounts[i];
		}
		
		d_output[idx * NUM_CLASSES + i] = val;	//Calculate and set the mean for the current class.
	}
} //end meanByFeature

/*
 * Calculates the variance of each feature-class pair.
 * Puts the results in d_output.
 * d_output is arranged so the f-c mean is at the index f*NUM_CLASSES + c

 * input:
 * d_input is the data: s x f
 * classMeans: c x f
 * classes: c
 */
__global__
void varianceByFeature(double * d_input, double * d_output, double * classMeans, int * classes/*, int numClasses, int numSamples, int numFeatures*/)
{
	//classes input are 1-indexed, convert to 0-index

	//Index from 1 to NUM_FEATURES ie the feature this thread is concerned with
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	//Class # of each sample for fast access
	__shared__ int sharedClasses[NUM_SAMPLES];

	//Copy classes to shared memory, convert from 1 to 0 indexing
	for (int i = threadIdx.x; i < NUM_SAMPLES; i += blockDim.x)
	{
		sharedClasses[i] = classes[i] - 1;
	}

	__syncthreads();


	//if outside the bounds of the input array (extra threads are allocated to make sure we have enough)
	if (idx >= NUM_FEATURES)
		return;

	//containers for variances sums and counts of class occurences
	double sums[NUM_CLASSES];			//Array that holds the sum for each class. 
	double sumCounts[NUM_CLASSES];		//Array that holds the count for each class.

	//Initialize class sums to 0
	for (int i = 0; i < NUM_CLASSES; ++i)
	{
		sums[i] = 0.0;
		sumCounts[i] = 0;
	}

	//Add up the sums of each feature-class pair
	//Also count the occurrences of each feature-class pair
	for (int i = 0; i < NUM_SAMPLES; ++i)
	{
		//Subtract the class/feature mean from the current value, square it, and add this to the sum
		sums[sharedClasses[i]] += pow(d_input[idx*NUM_SAMPLES + i] - classMeans[idx*NUM_CLASSES + sharedClasses[i]], 2);
		sumCounts[sharedClasses[i]]++;		//Increment the number of samples in the current sample's class.
	}

	//Insert the means into the output array
	for (int i = 0; i < NUM_CLASSES; ++i)
	{
		double val = 0;
		if (sumCounts[i] > 0)
		{
			val = sums[i] / sumCounts[i];	//We've done the summing and squaring, now divide by the number of samples
		}
		
		d_output[idx * NUM_CLASSES + i] = val;	//Set the mean for the current class.
	}
} //end varianceByFeature



/*
 * Calculates the gaussian probability of each class/feature pair given the input

 * input:
 *  d_input: the input vector: f
 *  sampleIdx
 *  classMeans: the means for

 * output:f x c array of probabilities that will need to be reduced
 */
__global__
void gaussianClassifierByFeature(double * d_input, double2 * d_output, double * classMeans, double * classVariances/*, int numClasses, int numFeatures*/)
{
	int feature = threadIdx.x + blockIdx.x * gridDim.x;
	
	if (feature >= NUM_FEATURES)
		return;
		
	double mean=0, variance=0;
	double temp1, temp2;
	double input = d_input[feature]; //one feature per thread

	double prob = 1, power = 0;
	
	//CALCULATE AND OUTPUT THE P VALUE:
	for( int i = 0; i < NUM_CLASSES; i++)
	{
	    //Look up the mean and variance for the current class and feature.
	    mean = classMeans[feature * NUM_CLASSES + i];		
		variance = classVariances[feature * NUM_CLASSES + i];	
	
		if (variance != 0)
		{
			temp1 = (1 / sqrt(2 * M_PI * variance));
			temp2 = exp(-(pow(input - mean, 2)) / (2 * variance));

			//Probability is interpreted as prob * 10^power
			prob = temp1 * temp2;
			power = 0.0;
			
			if (!isfinite(prob) || prob == 0)
			{
				prob = 1.0;
				//power = 0.0;
			}

			if (prob > 1000 || prob < 0.001)
			{
				prob = 1;
				power = 0;
			}


			while (prob < 1 && prob > 0)
			{
				prob *= 10;
				power--;
			}

			while (prob > 10)
			{
				prob /= 10;
				power++;
			}
		}
		else
		{
			prob = 0;
			power = 0;
		}
		
		d_output[i * NUM_FEATURES + feature] = make_double2(prob, power);
	}
}


/*
 * Simple reduce function.
 * Input - c values, multiply them all
 * Some restrictions may be needed to keep number >0
 *
 * output: probability of class 
 */
__global__
void reduceToClassProbability(double2* input, double2* output, int inputSize)
{
	extern __shared__ double2 sdata[];

	int tid = threadIdx.x;
	int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;

	sdata[tid].x = 1;
	sdata[tid].y = 0;

	if ( i >= inputSize )
		return;


	// Load shared memory by block
	//double2 in = input[i];
	if (isfinite(input[i].x) && isfinite(input[i].y)
		&& isfinite(input[i + blockDim.x].x) && isfinite(input[i + blockDim.x].y)
		&& ((i + blockDim.x) < inputSize ) )
	{
		sdata[tid].x = input[i].x*input[i + blockDim.x].x;
		sdata[tid].y = input[i].y+input[i + blockDim.x].y;
	}
	else
	{
		sdata[tid].x = 1;
		sdata[tid].y = 0;
	}

	__syncthreads();
	
	//Do reduction in shared memory
	for (int s = blockDim.x / 2; s>0; s >>= 1)
	{
		if (tid < s)
		{
			if ( i + s < inputSize)
			{
				sdata[tid].x = sdata[tid].x * sdata[tid + s].x;
				sdata[tid].y = sdata[tid].y + sdata[tid + s].y;
			}

			while (isfinite(sdata[tid].x) && sdata[tid].x >= 10)
			{
				sdata[tid].x = sdata[tid].x / 10;
				sdata[tid].y++;
			}
			while (isfinite(sdata[tid].x) && sdata[tid].x > 0 && sdata[tid].x < 1)
			{
				sdata[tid].x = sdata[tid].x * 10;
				sdata[tid].y--;
			}
		}
		__syncthreads();
	}

	// Write result for this block to global memory
	if (tid == 0)
	{
		output[blockIdx.x].x = sdata[0].x;
		output[blockIdx.x].y = sdata[0].y;
	}
}

/*
 * Reduce to find the maximum of an array of double2s. Each double2 is interpreted as
 * x*10^y (xey)
 */
__global__
void findMaxProbability(double2* input, double2* output, int* out_index)
{
	extern __shared__ double2 sdata[];

	int tid = threadIdx.x;
	int i = blockIdx.x*blockDim.x + threadIdx.x;

	if (i < NUM_CLASSES && tid < NUM_CLASSES)
	{

		// Load shared memory by block
		sdata[tid] = input[i];
		__syncthreads();


		for (int s = blockDim.x / 2; s > 0; s >>= 1)
		{
			if (tid < s && (tid+s) < NUM_CLASSES)
			{
				int a = tid; int b = tid + s;

				if (compareDouble2Scientific(sdata[a], sdata[b]))
				{
					//a is greater, do nothing
				}
				else //b is greater
				{
					sdata[a].x = sdata[b].x;
					sdata[a].y = sdata[b].y;
				}
			}
			__syncthreads();
		}

		// Write result for this block to global memory
		if (tid == 0)
		{
			output[blockIdx.x] = sdata[0];
			out_index[0] = 0;
		}

		__syncthreads();
		
		//Copy out the index of the highest probability
		if (gridDim.x == 1)
		{
			if (input[i].x == sdata[0].x && input[i].y == sdata[0].y)
			{
				out_index[0] = i;
			}
		}
	}
}

/*
 * Compare two double2 values, treating them each like they're in scientific notation.
 * Returns true if a is greater than b or if b is 0
 */
__device__
bool compareDouble2Scientific(double2 a, double2 b)
{
	//don't consider values of (1,0)
	if (b.x == 1 && b.y == 0)
		return true;
	else if (a.x == 1 && a.y == 0)
		return false;
	

	//ignore values if they are zero or infinite
	if (b.x <= 0 || !isfinite(b.x))
		return true;
	else if (a.x <= 0 || !isfinite(a.x))
		return false;

	if (a.y > b.y)
		return true;
	else if (b.y > a.y)
		return false;
	else //if y's are equal
	{
		if (a.x >= b.x)
			return true;
		else
			return false;
	}
}