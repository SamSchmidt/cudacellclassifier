#include "Headers.h"
#include "NaiveBayes.h"
#include "utils.h"
#include <math.h>	//Needed for the pow(), sqrt(), and exp() functions.

__global__
void reduceKernel2DbyFeaturesToBlocksNaive( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures,
                int doublesPerThread ){

	extern __shared__ double data[];

    int feature = threadIdx.x + blockIdx.x * blockDim.x;
    int offsetCol = blockIdx.y * doublesPerThread;  
        //enough local memory to operate on "doublesPerThread" classes at a time


	//if blockDim is not a power of 2, then it has an odd factor and we must make sure we reduce odd elements
	//odd input means n/2 (round down) reduce steps, but the offset is n/2 round up
	//EXAMPLE
	//	0	1	2	3	4
	//	first reduce step is
	//	3	5	2	X	X

	int offset = (blockDim.x + 1) / 2; //round up
	int numReduces = blockDim.x / 2; //round down	

	for( int c = 0; c < doublesPerThread; c++){	//initialize data array
        if ((c + offsetCol) < numClasses){	    	
            data[ (threadIdx.x * doublesPerThread) + (c) ] 
                = d_input[ (feature * numClasses) + (c + offsetCol) ];
        }
	}

	//COMPLETE REDUCE over ALL CLASSES in subgroup
	while ( numReduces > 0 ){				//Keep reducing until numReduces needed is 0
		for( int c = 0; c < doublesPerThread; c++){		//Iterate through the classes.

            // bug: should be less than, not less-than-equal-to
			if (threadIdx.x < numReduces){
//                if (threadIdx.x == 0 && blockIdx.x == 0 && blockIdx.y == 0) 
//                    printf("numReduces:   %1d  offset : %1d      \n", numReduces, offset);

				//TODO THIS line causes the crash
//                printf("col: %1d x %1d a %1d b %1d \n", c, threadIdx.x, threadIdx.x *doublesPerThread + c, (threadIdx.x + offset) * doublesPerThread + c );
				data[threadIdx.x *doublesPerThread + c] = data[threadIdx.x * doublesPerThread+ c] 
									* data[(threadIdx.x + offset) * doublesPerThread + c]; 
			}
			//IF numReduces < offset, we don't need to do anything to the extra odd element; let it sit
		}		
        //both based on previous offset
		numReduces = offset / 2;
		offset = (offset + 1) / 2;	
	}


	//Write Block's reduce result for each class 
	//TODO this could be done more quickly 
    if (threadIdx.x == 0){
	    for( int c = 0; c < numClasses; c++){	
            if ((c + offsetCol) < numClasses){			
                d_output[blockIdx.x * numClasses + (c + offsetCol)] = data[c];
            }
		}

//        printf("%1d : data : %f : d_output : %f \n", blockIdx.x, d_output[ blockIdx.x * numClasses + 0 ], data[0]);
    }



}



void testReduce(double * h_input, double * h_output, int numClasses, int numFeatures){

	//int memory = BLOCK_SIZE * numClasses * sizeof(double);
    int doublesPerThread = MAX_SHARED;
    doublesPerThread /= DOUBLE_SIZE;
    doublesPerThread /= BLOCK_SIZE; 
	int memoryBlock = BLOCK_SIZE * doublesPerThread * sizeof(double);

    /*
    printf("memmory yo %1d\n", memory);
    printf("memmoryBlock yo %1d\n", memoryBlock);
    printf("upper bound %1d\n", memoryBlock/DOUBLE_SIZE);
    */

    //DESIGN: if we have more than doublesPerThread classes, we need multiple blocks in y dimension
	dim3 blockSize(BLOCK_SIZE,1,1);
	dim3 gridSize( ceil(numFeatures/BLOCK_SIZE),ceil(numClasses/doublesPerThread),1 );
    //gridSize = dim3(1,1,1);  //debug

	double* d_data, * d_ProbByBlock;
	//Malloc and copy device vars
	checkCudaErrors( cudaMalloc( &d_data, numFeatures * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_ProbByBlock, gridSize.x * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMemset( d_ProbByBlock, 0.0, gridSize.x * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMemcpy( d_data, h_input, numFeatures * numClasses * sizeof( double ), cudaMemcpyHostToDevice ) );
	reduceKernel2DbyFeaturesToBlocksNaive<<<gridSize, blockSize, memoryBlock>>>(d_data, d_ProbByBlock, numClasses, numFeatures, doublesPerThread);	
	checkCudaErrors( cudaGetLastError() );
	checkCudaErrors( cudaMemcpy( h_output, d_ProbByBlock, gridSize.x * numClasses * sizeof( double ), cudaMemcpyDeviceToHost ) );

}




