#include "Headers.h"
#include "NaiveBayes.h"
#include "utils.h"
#include <math.h>	//Needed for the pow(), sqrt(), and exp() functions.

const int MAX_SHARED = 49152;
const int DOUBLE_SIZE = 8;


__global__
void reduceKernel2DbyFeaturesToBlocks( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures){

	extern __shared__ double data[];
	int feature = threadIdx.x + blockIdx.x * blockDim.x;

	//if blockDim is not a power of 2, then it has an odd factor and we must make sure we reduce odd elements
	//odd input means n/2 (round down) reduce steps, but the offset is n/2 round up
	//EXAMPLE
	//	0	1	2	3	4
	//	first reduce step is
	//	3	5	2	X	X

	int offset = (blockDim.x + 1) / 2; //round up
	int numReduces = blockDim.x / 2; //round down	

	for( int c = 0; c < numClasses; c++){	//initialize data array
		data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
	}

/*
	//do first reduce and copy data at same time
	for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
		if (threadIdx.x <= numReduces){
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ]
					* d_input[ (feature + offset) * numClasses + c ];
		}
		else if (threadIdx.x <= offset) {
			//IFF blockDim.x is odd, then this if is triggered.  We must copy 1 element without reducing
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
		}
	}
*/
/*
	//reduce loop control	
	offset = (offset + 1) / 2;
	numReduces = offset / 2;
	//COMPLETE REDUCE over ALL CLASSES
	while ( numReduces > 0 ){				//Keep reducing until numReduces needed is 0
		for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
			if (threadIdx.x <= numReduces){
				//TODO THIS line causes the crash
				data[threadIdx.x * numClasses + c] = data[threadIdx.x * numClasses + c] 
									+ data[(threadIdx.x + offset) * numClasses + c]; 
			}
			//IF numReduces < offset, we don't need to do anything to the extra odd element; let it sit
		}		
		offset = (offset + 1) / 2;
		numReduces = offset / 2;		
	}
*/
/*
	//Write Block's reduce result for each class 
	//TODO this could be done more quickly 
	for( int c = 0; c < numClasses; c++){	
		if (threadIdx.x == 0){
			d_output[blockIdx.x * numClasses + c] = 147.0/(10+c); //data[c];
		}
	}
    printf("%1d : data : %f : d_output : %f \n", blockIdx.x, d_output[ blockIdx.x * numClasses + 0 ], data[0]);
*/
}

__global__
void reduceKernel2DbyFeaturesToBlocks2( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures){

	extern __shared__ double data[];
	int myID = threadIdx.x + blockIdx.x * blockDim.x;

	//if blockDim is not a power of 2, then it has an odd factor and we must make sure we reduce odd elements
	//odd input means n/2 (round down) reduce steps, but the offset is n/2 round up
	//EXAMPLE
	//	0	1	2	3	4
	//	first reduce step is
	//	3	5	2	X	X

	//int offset = (blockDim.x + 1) / 2; //round up
	//int numReduces = blockDim.x / 2; //round down	

	//testing theory
	int offset = (BLOCK_SIZE + 1) / 2; //round up
	int numReduces = BLOCK_SIZE/ 2; //round down	

	//do first reduce and copy data at same time
	for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
		if (threadIdx.x <= numReduces){
			data[threadIdx.x * numClasses + c] = d_input[ myID * numClasses + c ]
						* d_input[ (myID + offset) * numClasses + c ];
		}
		else if (threadIdx.x <= offset) {
			//IFF blockDim.x is odd, then this if is triggered.  We must copy 1 element without reducing
			data[threadIdx.x * numClasses + c] = d_input[ myID * numClasses + c ];
		}
	}

/*
	//reduce loop control	
	offset = (offset + 1) / 2;
	numReduces = offset / 2;
	//COMPLETE REDUCE over ALL CLASSES
	while ( numReduces > 0 ){				//Keep reducing until numReduces needed is 0
		for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
			if (threadIdx.x <= numReduces){
				//TODO THIS line causes the crash
				data[threadIdx.x * numClasses + c] = data[threadIdx.x * numClasses + c] 
									+ data[(threadIdx.x + offset) * numClasses + c]; 
			}
			//IF numReduces < offset, we don't need to do anything to the extra odd element; let it sit
		}		
		offset = (offset + 1) / 2;
		numReduces = offset / 2;		
	}

	//Write Block's reduce result for each class 
	//TODO this could be done more quickly 
	for( int c = 0; c < numClasses; c++){	
		if (threadIdx.x == 0){
			d_output[blockIdx.x * numClasses + c] = data[c];
		}
	}
*/
}


void testReduce(double * h_input, double * h_output, int numClasses, int numFeatures){
	//int grid

	int memory = BLOCK_SIZE * numClasses * sizeof(double);
    printf("memmory yo %1d\n", memory);
    int doublesPerThread = MAX_SHARED / DOUBLE_SIZE / BLOCK_SIZE;  //32
	int memoryBlock = BLOCK_SIZE * doublesPerThread * sizeof(double);
    printf("memmoryBlock yo %1d\n", memoryBlock);
    int yBlocks = ceil(numClasses/doublesPerThread);

    //DESIGN: if we have more than doublesPerThread classes, we need multiple blocks in y dimension
	dim3 blockSize(BLOCK_SIZE,yBlocks,1);
	dim3 gridSize( ceil(numFeatures/BLOCK_SIZE),1,1 );



	double* d_data, * d_ProbByBlock;
	//Malloc and copy device vars
	checkCudaErrors( cudaMalloc( &d_data, numFeatures * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_ProbByBlock, gridSize.x * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMemset( d_ProbByBlock, 0.0, gridSize.x * numClasses * sizeof( double ) ) );
	checkCudaErrors( cudaMemcpy( d_data, h_input, numFeatures * numClasses * sizeof( double ), cudaMemcpyHostToDevice ) );
//	reduceKernel2DbyFeaturesToBlocksNaive<<<gridSize, blockSize,numClasses * sizeof( double )>>>(d_data, d_ProbByBlock, numClasses, numFeatures);	
	checkCudaErrors( cudaGetLastError() );
	checkCudaErrors( cudaMemcpy( h_output, d_ProbByBlock, gridSize.x * numClasses * sizeof( double ), cudaMemcpyDeviceToHost ) );

}



__global__
void reduceKernel2DbyFeaturesToBlocksMemorySave( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures){

    int doublesPerThread = MAX_SHARED / DOUBLE_SIZE / BLOCK_SIZE;  //32

	extern __shared__ double data[];

    
	int feature = threadIdx.x + blockIdx.x * blockDim.x;

	//if blockDim is not a power of 2, then it has an odd factor and we must make sure we reduce odd elements
	//odd input means n/2 (round down) reduce steps, but the offset is n/2 round up
	//EXAMPLE
	//	0	1	2	3	4
	//	first reduce step is
	//	3	5	2	X	X

	int offset = (blockDim.x + 1) / 2; //round up
	int numReduces = blockDim.x / 2; //round down	

	for( int c = 0; c < numClasses; c++){	//initialize data array
		data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
	}

/*
	//do first reduce and copy data at same time
	for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
		if (threadIdx.x <= numReduces){
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ]
					* d_input[ (feature + offset) * numClasses + c ];
		}
		else if (threadIdx.x <= offset) {
			//IFF blockDim.x is odd, then this if is triggered.  We must copy 1 element without reducing
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
		}
	}
*/
/*
	//reduce loop control	
	offset = (offset + 1) / 2;
	numReduces = offset / 2;
	//COMPLETE REDUCE over ALL CLASSES
	while ( numReduces > 0 ){				//Keep reducing until numReduces needed is 0
		for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
			if (threadIdx.x <= numReduces){
				//TODO THIS line causes the crash
				data[threadIdx.x * numClasses + c] = data[threadIdx.x * numClasses + c] 
									+ data[(threadIdx.x + offset) * numClasses + c]; 
			}
			//IF numReduces < offset, we don't need to do anything to the extra odd element; let it sit
		}		
		offset = (offset + 1) / 2;
		numReduces = offset / 2;		
	}
*/
/*
	//Write Block's reduce result for each class 
	//TODO this could be done more quickly 
	for( int c = 0; c < numClasses; c++){	
		if (threadIdx.x == 0){
			d_output[blockIdx.x * numClasses + c] = 147.0/(10+c); //data[c];
		}
	}
    printf("%1d : data : %f : d_output : %f \n", blockIdx.x, d_output[ blockIdx.x * numClasses + 0 ], data[0]);
*/
}

__global__
void reduceKernel2DbyFeaturesToBlocksNaive( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures){

	extern __shared__ double data[];

    
	int feature = threadIdx.x + blockIdx.x * blockDim.x;

	//if blockDim is not a power of 2, then it has an odd factor and we must make sure we reduce odd elements
	//odd input means n/2 (round down) reduce steps, but the offset is n/2 round up
	//EXAMPLE
	//	0	1	2	3	4
	//	first reduce step is
	//	3	5	2	X	X

	int offset = (blockDim.x + 1) / 2; //round up
	int numReduces = blockDim.x / 2; //round down	

	for( int c = 0; c < numClasses; c++){	//initialize data array
		data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
	}

/*
	//do first reduce and copy data at same time
	for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
		if (threadIdx.x <= numReduces){
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ]
					* d_input[ (feature + offset) * numClasses + c ];
		}
		else if (threadIdx.x <= offset) {
			//IFF blockDim.x is odd, then this if is triggered.  We must copy 1 element without reducing
			data[threadIdx.x * numClasses + c] = d_input[ feature * numClasses + c ];
		}
	}
*/
/*
	//reduce loop control	
	offset = (offset + 1) / 2;
	numReduces = offset / 2;
	//COMPLETE REDUCE over ALL CLASSES
	while ( numReduces > 0 ){				//Keep reducing until numReduces needed is 0
		for( int c = 0; c < numClasses; c++){		//Iterate through the classes.
			if (threadIdx.x <= numReduces){
				//TODO THIS line causes the crash
				data[threadIdx.x * numClasses + c] = data[threadIdx.x * numClasses + c] 
									+ data[(threadIdx.x + offset) * numClasses + c]; 
			}
			//IF numReduces < offset, we don't need to do anything to the extra odd element; let it sit
		}		
		offset = (offset + 1) / 2;
		numReduces = offset / 2;		
	}
*/
/*
	//Write Block's reduce result for each class 
	//TODO this could be done more quickly 
	for( int c = 0; c < numClasses; c++){	
		if (threadIdx.x == 0){
			d_output[blockIdx.x * numClasses + c] = 147.0/(10+c); //data[c];
		}
	}
    printf("%1d : data : %f : d_output : %f \n", blockIdx.x, d_output[ blockIdx.x * numClasses + 0 ], data[0]);
*/
}

