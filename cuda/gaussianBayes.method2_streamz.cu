#include "Headers.h"
#include "NaiveBayes.h"
#include "utils.h"
#include "print.h"
#include <math.h>	//Needed for the pow(), sqrt(), and exp() functions.

__global__
void reduceKernel2DbyFeaturesToBlocksNaive( 
				double * d_input,  // NUM_FEATURES X NUM_CLASSES
				double * d_output, // ceil(NUM_FEATURES/BLOCK_SIZE) X NUM_CLASSES
				                   // recurse until 1 X NUM_CLASSES
				int numClasses,
				int numFeatures,
                int doublesPerThread );

__global__
void meanByFeature( double * d_input, double * d_output, int * classes, int numClasses, int numSamples ){ 
	// total threadIdx (including block) is the feature number
	// classes must be ZERO INDEXED, which may not be native enumeration schema for the data

	int feature = threadIdx.x + blockIdx.x * blockDim.x;
	
	extern __shared__ int classID[];

	//SET CLASS IDS FOR EACH SAMPLE
	//if numsamples > blockDim.x, we need loop to copy all
	for( int z = 0; z < (numSamples - 1) / blockDim.x + 1; z++){	//copy all the classes to copy
		int k = threadIdx.x + z * blockDim.x;	//k is offset by blockDim.x to capture all ClassID when copying
		if ( k < numSamples )			//Make sure that we are not reading off the array
			classID[k] = classes[k]-1;	//Set the sample's class ID.  Copy to shared memory
	}

	double * sum = new double[numClasses];		//Array that holds the sum for each class. 
	int * sumCount = new int[numClasses];		//Array that holds the count for each class.

	//INITIALIZE CLASS SUMS
	for( int k = 0; k < numClasses; k++){		//Loop through the classes.
		sum[k] = 0.0;				//Set the current class' sum to 0.0.
		sumCount[k] = 0;			//Set the current class' count to 0.
	}

	//CALCULATE CLASS SUMS
	for( int k = 0; k < numSamples; k++){		//Loop through the samples.
		sum[  classID[k]  ] = sum[  classID[k]  ] + d_input[feature * numSamples + k];	//Add the current sample to the sum for its class.
		(sumCount[  classID[k]  ])++;		//Increment the number of samples in the current sample's class.
	}

	//OUTPUT MEAN
	for( int k = 0; k < numClasses; k++){		//Loop through the classes.
		d_output[feature * numClasses + k] = sum[k] / sumCount[k];	//Calculate and set the mean for the current class.
	}
} //end meanByFeature



__global__
void varianceByFeature(double * d_input, double * d_output, double * classMeans, int * classes, int numClasses, int numSamples){
	int feature = threadIdx.x + blockIdx.x * blockDim.x;
	extern __shared__ int classID[];
	//SET CLASS IDS FOR EACH SAMPLE
	//if numsamples > blockDim.x, we need loop to copy all
	for( int z = 0; z < (numSamples - 1) / blockDim.x + 1; z++){	//copy all the classes to copy
		int k = threadIdx.x + z * blockDim.x;	//k is offset by blockDim.x to capture all ClassID when copying
		if ( k < numSamples )			//Make sure that we are not reading off the array
			classID[k] = classes[k]-1;	//Set the sample's class ID.  Copy to shared memory
	}


	double * variance = new double[numClasses];	//Array that holds the sum for each class. 
	int * sumCount = new int[numClasses];		//Array that holds the count for each class.

	//INITIALIZE CLASS VARIANCE
	for( int k = 0; k < numClasses; k++){		//Loop through the classes.
		variance[k] = 0.0;			//Set the current class' variance to 0.0.
        sumCount[k] = 0;
	}

	//CALCULATE THE VARIANCE:
	for(int i = 0; i < numSamples; i++){	//Loop through the samples.
		int myClass = classID[i];	//Get the current sample's class.
		variance[myClass] += pow(d_input[feature * numSamples + i] - classMeans[myClass], 2);	
		(sumCount[  myClass  ])++;
	}

	//OUTPUT THE VARIANCE
	for( int k = 0; k < numClasses; k++){		//Loop through the classes.
 
        if(sumCount[k]>2){
    		d_output[feature * numClasses + k] = variance[k] / sumCount[k];	//Calculate and set the mean for the current class.
        }else{
            printf("class %d feature %d sumCount %d varianceSum %f\n", k, feature, sumCount[k], variance[k]);
            d_output[feature * numClasses + k] = 1; 
        } 

        if( d_output[feature * numClasses + k] != d_output[feature * numClasses + k] ){ //nan
            printf("variance nan found: %f varSum: %f count: %d class: %d\n", d_output[feature * numClasses + k], variance[k], sumCount[k],k+1); 
            printf("input array\n");
            for (int z=0; z<numSamples; z++){
                if(classID[z]-1 == k )
                    printf("class %d input %f\n", classID[z], d_input[feature * numSamples + z]);
            } //end for
        } //end if

	}
}

#define PI 3.1415926535897
__global__
void gaussianClassifierByFeature(double * d_input, double * d_output, int sampleIdx, double * classMeans, double * classVariances, int numClasses, int doublesPerThread)
{
	int feature = threadIdx.x + blockIdx.x * blockDim.x;
	double input = d_input[feature * NUM_SAMPLES + sampleIdx];
	double mean , variance;
    int classID;
    extern __shared__ double classifier[];

	//CALCULATE AND OUTPUT THE P VALUE:
	for( int k = 0; k < doublesPerThread; k++){				//Loop through the classes.
        classID = (blockIdx.y * doublesPerThread + k) ;
		mean = classMeans[  (feature * numClasses) + classID ];		
            //Look up the mean for the current class and feature.
		variance = classVariances[feature * numClasses + classID];	
            //Look up the variance for the current class and feature

        if( classID < numClasses )
    		classifier[ feature * doublesPerThread + k ] = 
                PROB_OFFSET * (1 / sqrt(2 * PI * variance)) * exp(-(pow(input - mean, 2)) / (2 * variance));

	}

    d_output[feature * numClasses + k];

}

double* d_data;
int* d_classes;

double* d_means;
double* d_variances;
double* d_stdDev;
double* d_pValues;
double* d_testData;
double * d_intermediate;
double * d_intermediate2;
double * d_fuzzy;
double * d_output;
//double * h_fuzzy;


void initializeCudaVars( NaiveBayes nb ){

	//Malloc and copy device vars
	checkCudaErrors( cudaMalloc( &d_data, NUM_FEATURES * NUM_SAMPLES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_classes, NUM_SAMPLES * sizeof( int ) ) );
	checkCudaErrors( cudaMalloc( &d_means, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_variances, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_stdDev, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_pValues, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_testData, NUM_FEATURES * sizeof( double ) ) );
	//checkCudaErrors( cudaMemset( d_variances, -1.0, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
	checkCudaErrors( cudaMemcpy( d_data, nb.get1Ddata(), NUM_FEATURES * NUM_SAMPLES * sizeof( double ), cudaMemcpyHostToDevice ) );
	checkCudaErrors( cudaMemcpy( d_classes, nb.getClasses(), NUM_SAMPLES * sizeof( int ), cudaMemcpyHostToDevice ) );
	checkCudaErrors( cudaMemcpy( d_testData, nb.data[0], NUM_FEATURES * sizeof( double ), cudaMemcpyHostToDevice ) );
}

void classifierStats()
{
	//Set block and grid size:
	dim3 blockSize(192,1,1);
	dim3 gridSize = (ceil( NUM_FEATURES/blockSize.x ),1,1);

	//Get means	
	meanByFeature<<<gridSize, blockSize, NUM_SAMPLES * sizeof(int)>>>( d_data, d_means, d_classes, NUM_CLASSES, NUM_SAMPLES );
	checkCudaErrors(cudaGetLastError());
	
	//Get stdev
//	stdDevByFeature<<<gridSize, blockSize, NUM_SAMPLES * sizeof(int)>>>( d_data, d_stdDev, d_means, d_classes, NUM_CLASSES, NUM_SAMPLES);
//	checkCudaErrors(cudaGetLastError());

	varianceByFeature<<<gridSize, blockSize, NUM_SAMPLES * sizeof(int)>>>( d_data, d_variances, d_means, d_classes, NUM_CLASSES, NUM_SAMPLES);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
    //printf("d_variances\n");
    //print_Array(d_variances, NUM_CLASSES * NUM_FEATURES);
	//checkCudaErrors( cudaMemset( d_stdDev, 0.02, NUM_CLASSES * NUM_FEATURES * sizeof( double ) ) );
}

void runClassifyTest(double * h_fuzzy){

    int numClasses = NUM_CLASSES;
    int numFeatures = NUM_FEATURES;

    int doublesPerThread = MAX_SHARED;
    doublesPerThread /= DOUBLE_SIZE;
    doublesPerThread /= BLOCK_SIZE;  
	int memoryBlock = BLOCK_SIZE * doublesPerThread * sizeof(double);
    //DESIGN: if we have more than doublesPerThread classes, we need multiple blocks in y dimension
	dim3 blockSize(BLOCK_SIZE,1,1);
	dim3 gridSize( ceil(numFeatures/BLOCK_SIZE),ceil(numClasses/doublesPerThread),1 );

    dim3 gridSize2( ceil(ceil(numFeatures/BLOCK_SIZE)/BLOCK_SIZE), ceil(numClasses/doublesPerThread), 1);

    dim3 gridSize3( 2, ceil(numClasses/doublesPerThread), 1);


	checkCudaErrors( cudaMalloc( &d_intermediate, NUM_FEATURES * NUM_CLASSES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_intermediate2, 2 * NUM_CLASSES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_fuzzy, NUM_CLASSES * sizeof( double ) ) );
	checkCudaErrors( cudaMalloc( &d_output, NUM_CLASSES * NUM_SAMPLES * sizeof( double ) ) );

    cudaStream_t * streamz = new cudaStream_t[NUM_SAMPLES];

	for (int k=0; k< 1; k++){
		//Calculates p Value for each Feature x Class for the input test vector	(NUM_FEATURE X 1)
		gaussianClassifierByFeature<<<gridSize, blockSize>>>(d_data, d_pValues, k, d_means, d_variances, NUM_CLASSES, doublesPerThread);
        printf("d_pValues\n");
        print_Array(d_pValues, 5);
		//Reduce each class to one posterior probability by reducing the features pValues
		//I.e. Reduce the features for each class to a single probability.
		//This kernel does block reduction only, so we have to call twice due to array size

		//int mySize =  BLOCK_SIZE * NUM_CLASSES * sizeof(double);//NUM_CLASSES*NUM_FEATURES*sizeof(double);

		reduceKernel2DbyFeaturesToBlocksNaive<<<gridSize, blockSize, memoryBlock>>>( d_pValues, d_intermediate, NUM_CLASSES, NUM_FEATURES, doublesPerThread);
        //input:            d_pValues       NUM_FEATURES x NUM_CLASSES
        //output:           d_intermediate  (NUM_FEATURES/192) x NUM_CLASSES
        //numClasses:       NUM_CLASSES
        //numFeatures:      NUM_FEATURES
        //doublesPerThread: MAX_SHARED / BLOCK_SIZE / DOUBLE_SIZE
        printf("d_intermediate\n");
        //print_Array(d_intermediate, NUM_FEATURES * NUM_CLASSES);
        print_Array(d_intermediate, 5);
		
	    reduceKernel2DbyFeaturesToBlocksNaive<<<gridSize2, blockSize, memoryBlock>>>( d_intermediate, d_intermediate2, NUM_CLASSES, gridSize.x, doublesPerThread);
        //input:            d_intermediate  (NUM_FEATURES/192) x NUM_CLASSES
        //output:           d_fuzzy         (NUM_FEATURES/192)/192 x NUM_CLASSES
        //                                    2 x NUM_CLASSES
        //numClasses:       NUM_CLASSES
        //numFeatures:      ceil(NUM_FEATURES/192)
        //doublesPerThread: MAX_SHARED / BLOCK_SIZE / DOUBLE_SIZE
        printf("d_intermediate2\n");
        print_Array(d_intermediate2, 5);

	    reduceKernel2DbyFeaturesToBlocksNaive<<<gridSize3, blockSize, memoryBlock>>>( d_intermediate2, d_fuzzy, NUM_CLASSES, gridSize.x, doublesPerThread);
        //input:            d_intermediate  (NUM_FEATURES/192) x NUM_CLASSES
        //output:           d_fuzzy         (NUM_FEATURES/192)/192 x NUM_CLASSES
        //                                    2 x NUM_CLASSES
        //numClasses:       NUM_CLASSES
        //numFeatures:      2
        //doublesPerThread: MAX_SHARED / BLOCK_SIZE / DOUBLE_SIZE
        printf("d_fuzzy\n");
        print_Array(d_fuzzy, 5);

		checkCudaErrors( cudaMemcpy( &d_output[k*NUM_CLASSES], d_fuzzy, NUM_CLASSES * sizeof( double ), cudaMemcpyDeviceToDevice ) );	
	}
	//return sample to host to verify correct function
	checkCudaErrors( cudaMemcpy( h_fuzzy, d_fuzzy, NUM_CLASSES * sizeof( double ), cudaMemcpyDeviceToHost ) );	


//	cudaDeviceSynchronize();
//	checkCudaErrors(cudaGetLastError());
}

