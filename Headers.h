/* CUDA CellClassifier - Classify cells in parallel using CUDA
Copyright(C) <2015>  <Samuel Schmidt>

This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>
*/
//
//  Headers.h
//
//Contains headers, global variables

#ifndef __serialNaiveBayes__Headers__
#define __serialNaiveBayes__Headers__

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <string>
#include <cstring>

#include <vector>
#include <cmath>
#include <chrono>

#define M_PI 3.14159265358979323846


#define NUM_FEATURES 39327
#define NUM_SAMPLES 479
#define NUM_CLASSES 114
#define CLASS_FILE "data/classesOnly.csv"
#define DATA_FILE "data/dataCleaned.csv"

//FOR TEST
//#define NUM_FEATURES 9
//#define NUM_SAMPLES 3
//#define NUM_CLASSES 10
//#define DATA_FILE "data/dataTest.csv"
//#define CLASS_FILE "data/classesOnlyTest.csv"


#define PROB_OFFSET 1.0

#define MAX_SHARED 49152;
#define DOUBLE_SIZE 8;

#define BLOCK_SIZE 192
//#define HALF_BLOCK_SIZE 96

template <typename T>
void printArray(T** array, int dim1, int dim2 );

#endif //Headers
