/* CUDA CellClassifier - Classify cells in parallel using CUDA
	Copyright(C) <2015>  <Samuel Schmidt>

	This program is free software : you can redistribute it and / or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.If not, see <http://www.gnu.org/licenses/>
*/

//kernel.cu
//
//Main function file. Also contiains a few testing and timing functions.
//Program starts at main()

#include "Headers.h"
#include "cudaHeaders.h"

using namespace std;
using namespace std::chrono;


double* d_testData;

void testOnEntireSet(NaiveBayes nb);
void testOnEntireSetAndTime(NaiveBayes nb);


int main()
{
	////////////////////
	///INITIALIZATION///
	////////////////////
	NaiveBayes nb;// = new NaiveBayes();

	//Allocate the heap space we need
	nb.allocateArrays();

	//Get training classes and data
	if (nb.readClassesFromFile() == false)
		return 1;

	if (nb.readDataFromFile() == false)
		return 2;



	///////////
	///TRAIN///
	///////////

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	//Calculate values needed for gaussian distribution
	nb.calculateClassProbabilities();
	nb.calculateMeans();
	nb.calculateVariances();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	long  durationSerialTrain = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();


	t1 = high_resolution_clock::now();
	initializeCudaVars(nb);
	classifierStats();
	t2 = high_resolution_clock::now();
	long  durationParallelTrain = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();



	//////////
	///TEST///
	//////////
	//Test function for classifiers
	testOnEntireSetAndTime( nb );

	nb.finish();


	printf("Serial Train Time: %d\nParallel Train Time%d\n", durationSerialTrain, durationParallelTrain);
	printf("\nend\n");
	getchar();
	return 0;
}

/*
 * Train classifiers on the entire test dataset
 * Classify vectors from this set and and compare results, including 
 */
void testOnEntireSet(NaiveBayes nb)
{
	double numSamplesToTest = NUM_SAMPLES;

	double2 * h_fuzzy = new double2[NUM_CLASSES];


	int serialClass = 0, parallelClass = 0, realClass = 0;
	double serialCorrect = 0, parallelCorrect = 0;
	printf("testing on entire set (1-indexed)\n");

	for (int i = 0; i < numSamplesToTest; ++i)
	{
		//serial
		serialClass = nb.classifyVector(nb.data[i]);

		//parallel
		parallelClass = gaussianClassify(h_fuzzy, nb.classProbs, nb, nb.data[i]);

		realClass = nb.getClasses()[i];
		if (serialClass == realClass)
			serialCorrect++;
		if (parallelClass == realClass)
			parallelCorrect++;
		printf("sample %d. Real Class:%d\tSerial:%d\tParallel:%d\n", i, nb.getClasses()[i], serialClass, parallelClass);
	}

	printf("Serial Success Rate: %f\nSerial Correct:%f\nParallel SuccessRate:%f\nParallel Correct:%f\n", serialCorrect / numSamplesToTest, 
		serialCorrect, parallelCorrect / numSamplesToTest, parallelCorrect);


	delete[] h_fuzzy;
}

/*
* Train classifiers on the entire test dataset
* Classify vectors from this set and and compare results
*
* Time the parallel classifications versus the serials
*/
void testOnEntireSetAndTime(NaiveBayes nb)
{
	double numSamplesToTest = NUM_SAMPLES;//50

	double2 * h_fuzzy = new double2[NUM_CLASSES];


	int serialClass = 0, parallelClass = 0, realClass = 0;
	double serialCorrect = 0, parallelCorrect = 0;
	printf("testing on entire set (1-indexed)\n");


	//Time serial classification
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (int i = 0; i < numSamplesToTest; ++i)
	{
		//serial
		serialClass = nb.classifyVector(nb.data[i]);

		realClass = nb.getClasses()[i];

		if (serialClass == realClass)
			serialCorrect++;

		printf("sample %d. Real Class:%d\tSerial:%d\n", i, nb.getClasses()[i], serialClass);
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	long  durationSerial = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

	t1 = high_resolution_clock::now();
	for (int i = 0; i < numSamplesToTest; ++i)
	{
		//parallel
		parallelClass = gaussianClassify(h_fuzzy, nb.classProbs, nb, nb.data[i]);

		realClass = nb.getClasses()[i];
		
		if (parallelClass == realClass)
			parallelCorrect++;

		printf("sample %d. Real Class:%d\tParallel:%d\n", i, nb.getClasses()[i], parallelClass);
	}
	t2 = high_resolution_clock::now();
	long durationParallel = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();


	printf("Serial Success Rate: %f\nSerial Correct:%f\nParallel SuccessRate:%f\nParallel Correct:%f\n", serialCorrect / numSamplesToTest,
		serialCorrect, parallelCorrect / numSamplesToTest, parallelCorrect);

	printf("Serial Time: %d\nParallel Time%d\n", durationSerial, durationParallel);


	delete[] h_fuzzy;
}