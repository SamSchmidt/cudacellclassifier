/* CUDA CellClassifier - Classify cells in parallel using CUDA
Copyright(C) <2015>  <Samuel Schmidt>

This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>
*/

//
//  NaiveBayes.cpp
//	Contains source code for NaiveBayes.
//	This is the serial algorithm that is used as a baseline for 
//    the CUDA parallel algorithm.

#include "NaiveBayes.h"

#define _USE_MATH_DEFINES

using namespace std;

template <typename T>
void printArray(T** array, int dim1, int dim2 )
{
    for ( int i=0; i<dim1; ++i )
    {
        for ( int j=0; j<dim2; ++j )
        {
            cout << array[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

//Basic constructor
NaiveBayes::NaiveBayes()
{
    for ( int i=0; i<NUM_CLASSES; ++i )
    {
        data = NULL;
        means = NULL;
        variances = NULL;
    }
}

//Class file is a single row with the class number of each sample in appropriate column
bool NaiveBayes::readClassesFromFile()
{
    ifstream inFile;
    inFile.open(CLASS_FILE);
    if ( !inFile.is_open() )
    {
        cout << "error opening file: " << CLASS_FILE;
		return false;
    }

    
    string classStr;
    
    int i=0;
    getline(inFile, classStr, ',');
    while (!inFile.fail())
    {
        int classInt = atoi(classStr.c_str());
        
        classes[i] = classInt;
        classOccurrences[classInt-1]++;
        
        getline(inFile, classStr, ',');
        ++i;
    }
    
    inFile.close();
    
    return true;
}

void NaiveBayes::allocateArrays()
{
    classes = new int[NUM_SAMPLES];

    data1D = new double[NUM_SAMPLES * NUM_FEATURES];

    classOccurrences = new int[NUM_CLASSES];
    classProbs = new double[NUM_CLASSES];
    for ( int i=0; i<NUM_CLASSES; ++i )
    {
        classOccurrences[i] = 0;
        classProbs[i] = 0;
    }


    data = new double*[NUM_SAMPLES];
    for( int i=0; i<NUM_SAMPLES; ++i )
    {
        data[i] = new double[NUM_FEATURES];
    }
    
    means = new double*[NUM_CLASSES];
    variances = new double*[NUM_CLASSES];

    for( int i=0; i<NUM_CLASSES; ++i )
    {
        means[i] = new double[NUM_FEATURES];
        variances[i] = new double[NUM_FEATURES];
        
        for ( int j=0; j<NUM_FEATURES; ++j )
        {
            means[i][j] = 0;
            variances[i][j] = 0;
        }
    }
}

void NaiveBayes::deleteArrays()
{
    delete[] classes;
    delete[] data1D;
	delete[] classOccurrences;
	delete[] classProbs;

    for( int i=0; i<NUM_SAMPLES; ++i )
    {
        delete[] data[i];
    }
	delete[] data;

    for( int i=0; i<NUM_CLASSES; ++i )
    {
        delete[] means[i];
		delete[] variances[i];
    }
	delete[] means;
	delete[] variances;
}

bool NaiveBayes::readDataFromFile()
{
    ifstream inFile;
    inFile.open(DATA_FILE);
    if ( !inFile.is_open() )
    {
        cout << "error opening file: " << DATA_FILE;
        return false;
    }

    string classStr;
    
    int featureNum=0; //row of the file
    int index1d=0;

    char str[10000];
    inFile.getline(str, 10000, '\r');
    while (!inFile.fail())
    {
        string plusStr = str;
        
        char* pch;
        pch = strtok(str, ",");
        int sampleNum=0;    //column of the file (cell)
        while ( pch != NULL )
        {
            double dataVal = strtof(pch, NULL);
            
            data[sampleNum][featureNum] = dataVal;

            data1D[index1d] = dataVal;

            pch = strtok(NULL, ",");
            ++sampleNum;
            ++index1d;
        }
        
        inFile.getline(str, 10000, '\r');
        
        ++featureNum;
    }
    
    inFile.close();

    return true;
}

//Within each class, calculates the mean of each feature
void NaiveBayes::calculateMeans()
{
    double sum = 0;
    
    //sum up all features for each class
    for ( int sampleNum=0; sampleNum<NUM_SAMPLES; ++sampleNum )
        {
            for ( int featureNum=0; featureNum<NUM_FEATURES; ++featureNum )
            {
                sum = data[sampleNum][featureNum];
                means[classes[sampleNum]-1][featureNum] += data[sampleNum][featureNum];
            }
        }
    
    //divide by the number of classes
    for ( int classNum=0; classNum<NUM_CLASSES; ++classNum )
    {
        for ( int featureNum=0; featureNum<NUM_FEATURES; ++featureNum )
        {
            means[classNum][featureNum] /= classOccurrences[classNum];
        }
    }
}

//Within each class, calculates the variance of each feature
void NaiveBayes::calculateVariances()
{
    //do sum
    for ( int sampleNum=0; sampleNum<NUM_SAMPLES; ++sampleNum )
    {
        for ( int featureNum=0; featureNum<NUM_FEATURES; ++featureNum )
        {
            double test = pow(data[sampleNum][featureNum] - means[classes[sampleNum]-1][featureNum], 2);
            variances[classes[sampleNum]-1][featureNum] +=  test;
        }
    }
    
    //divide by the number of classes
    for ( int classNum=0; classNum<NUM_CLASSES; ++classNum )
    {
        for ( int featureNum=0; featureNum<NUM_FEATURES; ++featureNum )
        {
            variances[classNum][featureNum] /= classOccurrences[classNum];
        }
    }
}


//TODO: Use 1-smoothing here?
void NaiveBayes::calculateClassProbabilities()
{
    for ( int i=0; i<NUM_CLASSES; ++i )
    {
        classProbs[i] = (double)classOccurrences[i]/NUM_SAMPLES;
    }
}

//Wrapper for heap de-allocation
void NaiveBayes::finish()
{
    deleteArrays();
}

int NaiveBayes::classifyVector( double* input)
{
    vector<double> posteriors;      //scaled posterior probability of each class between 1 and 10
    vector<double> powers;          //posterior = posteriors[i] * 10^powers[i]
    
    double conditionals = 1;         //calculate the posterior by multiplying conditionals

    //temporary variables
    int power = 0;
    double mean=0, variance=0;
    
	//printf("serial classify output\n");
    for ( int classNum=0; classNum<NUM_CLASSES; ++classNum )
    {
        for ( int featureNum=0; featureNum<NUM_FEATURES; ++featureNum)
        {
            mean = means[classNum][featureNum];
            variance = variances[classNum][featureNum];
            
            double d = gaussianProbability( input[ featureNum ], mean, variance, power );

			conditionals *= d;

			scaleProbability(conditionals, power);
        }
        
        posteriors.push_back( conditionals );
        powers.push_back( power );
        conditionals = 1;
        power = 0;
    }
    
    //Find max of powers
    int maxClass = findMaxClass( powers, posteriors );

    return maxClass + 1;
}


void NaiveBayes::scaleProbability( double& prob, int& power )
{
    if ( prob == 0 )
        return;
    
    while ( prob < 1 )
    {
        prob *= 10;
        power--;
    }
    while ( prob > 10 )
    {
        prob /= 10;
        power++;
    }
}


double NaiveBayes::gaussianProbability( double val, double mean, double var, int& power )
{
    //Temporary(?) fix
    //If the variance is 0, gaussian will try to divByZero
    if ( var == 0 )
        return 0;
    
    double ret = ( 1.0 / ( sqrt(2.0*M_PI*var) ) ) * exp( -1 * pow( (val-mean), 2.0 ) / ( 2*var ) );
        
    if( ret < .001  || ret > 1000)
        return 1;
    
    return ret;
}

int NaiveBayes::findMaxClass( vector<double> powers, vector<double> posteriors )
{
    vector<int> indices;
    int maxPower;
    
    //Initialize
    maxPower = powers[0];
    indices.push_back(0);
    
    //Find indices with the highest power
    for( int i=1; i<powers.size(); ++i )
    {
        if ( powers[i] > maxPower && posteriors[i] != 0 )
        {
            indices.clear();
            indices.push_back(i);
            maxPower = powers[i];
        }
        else if ( powers[i] == maxPower )
        {
            indices.push_back(i);
        }
    }
    
    double maxClass = indices[0];
    
    //Find the highest posterior out of those with the highest power
    for( int i=1; i<indices.size(); ++i )
    {
        if ( (posteriors[indices[i] ] > posteriors[maxClass]) || (!isfinite(posteriors[maxClass]) && isfinite(posteriors[indices[i]]) ) )
            maxClass = indices[i];
    }
    
    return maxClass;
}


///////////////////////
//GETTERS AND SETTERS//
///////////////////////
int* NaiveBayes::getClasses()
{
    return classes;
}

double* NaiveBayes::get1Ddata()
{
    return data1D;
}
