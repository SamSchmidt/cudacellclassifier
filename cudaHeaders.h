/* CUDA CellClassifier - Classify cells in parallel using CUDA
Copyright(C) <2015>  <Samuel Schmidt>

This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>
*/

//cudaHeaders.h
//
//Contains functions declarations and globals for CUDA code

#ifndef __serialNaiveBayes__cudaHeaders__
#define __serialNaiveBayes__cudaHeaders__

#include "NaiveBayes.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

void initializeCudaVars(NaiveBayes);
void classifierStats();
int gaussianClassify(double2 * h_fuzzy, double* classProbs, NaiveBayes nb, double* h_testData);


__global__ void meanByFeature(double * d_input, double * d_output, int * classes/*, int numClasses, int numSamples, int numFeatures*/);
__global__ void varianceByFeature(double * d_input, double * d_output, double * classMeans, int * classes/*, int numClasses, int numSamples, int numFeatures*/);
__global__ void gaussianClassifierByFeature(double * d_input, double2 * d_output, double * classMeans, double * classVariances/*, int numClasses, int numFeatures*/);
__global__ void reduceToClassProbability(double2* input, double2* output, int inputSize);
__global__ void findMaxProbability(double2* input, double2* output, int* out_index);

__device__ bool compareDouble2Scientific(double2 a, double2 b);


/*
* macro to serve as a CUDA check function (let's us know if there are CUDA errors in a call)
*/
#define cudaErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

#endif //#define __serialNaiveBayes__cudaHeaders__