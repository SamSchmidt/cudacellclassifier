CUDACellClassifier
==============
This project is the code for Samuel Schmidt's Master's Thesis Project at the University of Cincinnati.

The code is an algorithm to perform massively parallel Cell Classification on RNA-seq data. This way, the user can avoid feature selection and use the bulk (or all) of the data.

This project uses CUDA, an NVIDIA C/C++ add-on, which sends code to an NVIDIA graphics card to massively parallelize C/C++ functions.