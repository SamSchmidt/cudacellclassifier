/* CUDA CellClassifier - Classify cells in parallel using CUDA
Copyright(C) <2015>  <Samuel Schmidt>

This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>
*/

//  NaiveBayes.h

#ifndef __serialNaiveBayes__NaiveBayes__
#define __serialNaiveBayes__NaiveBayes__

#include "Headers.h"

class NaiveBayes
{
    int* classes;
    int* classOccurrences;
public:
    double* classProbs;
    double* data1D;
    
    double **means;     //means[i][j] = mean for class i, feature j
    double **variances;    //similar    
    double **data;      //data[i][j] = the data from sample i, feature j

    
    NaiveBayes();
    ~NaiveBayes() { return; }
    
    void allocateArrays();
    
    bool readClassesFromFile();
    bool readDataFromFile();
    void calculateClassProbabilities();
    
    void calculateMeans();
    void calculateVariances();
    
    int classifyVector( double * );
    
    void finish();

    //getters
    int* getClasses();
    double* get1Ddata();
    
private:
    double gaussianProbability(double, double, double, int&);
    void scaleProbability( double&, int& );
    
    int findMaxClass(std::vector<double>, std::vector<double> );

    void deleteArrays();
};

#endif /* defined(__serialNaiveBayes__NaiveBayes__) */
